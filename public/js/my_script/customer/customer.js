tabla = $('#tbl_customer_show').DataTable({


    "language": {
        "url": "/plugins/datatables/Spanish.json"
    },
    responsive : true,
    ajax: url_get_customer,
    dom: 'Bfrtip',
    buttons: [
        'excel', 'pdf'
    ],
    columns:
        [
            {data: 'nombre', name: 'nombre'},
            {data: 'apellidos', name: 'apellidos'},
            {data: 'tipo_contacto', name: 'tipo_contacto'},
            {data: 'identificacion', name: 'identificacion'},
            {data: 'tipo_identifacion', name: 'tipo_identifacion'},
            {data: 'alerta', name: 'alerta'},
            {data: 'edad', name: 'edad'},
            {data: 'fecha_creacion', name: 'fecha_creacion'},
            {data: 'id', name: 'id'}
        ],

    columnDefs: [{

        'targets': -1,
        'searchable': false,
        'orderable': false,
        'className': 'dt-body-center',
        'render': function (date, type, full, meta){
            return '<button onclick="edit('+full['id']+ ');" type="button" class="customer_edit" name="id" id = "id_customer" value="' +full['id']+ '">Editar</button>';

        }
    }]
});


function edit(id) {

}
