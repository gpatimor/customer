<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->string('nombre',50);
            $table->string('apellidos',50);
            $table->string('identificacion',15);
            $table->integer('tipo_identificacion_id');
            $table->foreign('tipo_identificacion_id')->references('id')->on('tipo_identificacion');
            $table->integer('tipo_contacto_id');
            $table->foreign('tipo_contacto_id')->references('id')->on('tipo_contacto');
            $table->integer('alerta_id');
            $table->foreign('alerta_id')->references('id')->on('tipo_contacto');
            $table->smallInteger('edad');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
