<?php

namespace App\Http\Controllers;
use App\Customer;
use Excel;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('customer.load');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       /* if ($request->hasFile('customer__file_load')) {
            try {
                Excel::load($request->file('customer__file_load')->getRealPath(), function ($reader){

                    foreach ($reader->get() as $key => $sheet) {
                        $nameSheet = $sheet->getTitle();
                        $dataSheet = $sheet->toArray();
                        dd($nameSheet);
                        if (count($dataSheet) != 0) {
                            $this->processData($dataSheet, $nameSheet);

                        }
                    }
                });

                }
            catch (\Exception $e) {

            }
        }*/

        return view('customer.load');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('customer.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function customer_get()
    {
        return datatables()
            ->of(Customer::all())
            ->toJson();
    }
}
