<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/registrer','HomeController@index')->name('register');
Route::get('/home', 'HomeController@index')->name('home');

/*
 * Route customer
 */

Route::get('/clientes/get','CustomerController@customer_get')->name('customer_get');
Route::resource('/clientes','CustomerController');

