@extends('layouts.app')

@section('title')
    Lista de clientes
    @endsection()

@section('content')
    <div id ="frm_customer_show" class="box" id="Document">

        <div class="box-body">
            <table class="table" id="tbl_customer_show">
                <theah>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Tipo contacto</th>
                    <th>idenfificación</th>
                    <th>Tipo indenficación</th>
                    <th>Alerta</th>
                    <th>Edad</th>
                    <th>Fecha creación</th>
                    <th>Ciudad nacimiento</th>
                    <th>Opción</th>
                </theah>
                <tbody>

                </tbody>

            </table>
        </div>
    </div>
    @endsection()

@section('script')
    <script>
        url_get_customer = '{{ route('customer_get') }}'
    </script>
    <script src="{{ asset('/js/my_script/customer/customer.js')}}"></script>
@endsection()
