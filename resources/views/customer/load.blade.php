@extends('layouts.app')

@section('title')
    Cargar cliente
    @endsection()

@section('content')
    <form method="POST" action="/clientes"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleFormControlFile1">Cargar clientes</label>
            <input type="file" class="form-control-file" name="customer__file_load" id="exampleFormControlFile1">
        </div>
        <button type="submit" class="btn btn-primary">Cargar</button>
    </form>
@endsection
